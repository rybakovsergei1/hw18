#include <iostream>
#include <cassert>

using namespace std;

class Stack
{
    int* numers;
    unsigned last_index;
    unsigned size = 6;

public:
    Stack()
    {
        last_index = 0;
        numers = new int[size];
    }

    ~Stack()
    {
        delete[] numers;
    }

    explicit Stack(unsigned s)
    {
        size = s;
        last_index = 0;
        numers = new int[size];
    }

    void push(unsigned new_element)
    {
        if (last_index >= size)
        {
            const auto tempArray = new int[size * 2];
            for (auto i = 0u; i < size; i++)
            {
                tempArray[i] = numers[i];
            }
            delete[] numers;
            numers = tempArray;
            size *= 2;
        }
        numers[last_index++] = new_element;
    }

    auto pop()
    {
        assert(last_index > 0);
        last_index--;
        return numers[last_index];
    }
};


int typeAndValNum()
{
    auto valid = false;
    int num;
    do
    {
        cin >> num;
        if (cin.good())
        {
            valid = true;
        }
        else
        {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            cout << "�� ����� �� �����" << endl;
        }
    } while (!valid);
    return num;
}

void lesson18()
{
    unsigned size = 0;
    do
    {
        cout << "������� ����� �� ������ ������?" << endl;
        size = typeAndValNum();
        if (size <= 1)
        {
            cout << "������� ����� ������ �������" << endl;
        }
    } while (size <= 1);
    Stack stack_object(size);

    cout << "������ ���� �����:" << endl;

    for (unsigned i = 0; i < size; i++)
    {
        stack_object.push(typeAndValNum());
    }

    cout << "������� �������: " << stack_object.pop() << endl;
    cout << "�������:" << endl;
    for (unsigned i = 1; i < size; i++)
    {
        cout << stack_object.pop() << endl;
    }
}


int main()
{
    setlocale(LC_ALL, "rus");
    lesson18();
}